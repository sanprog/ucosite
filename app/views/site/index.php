<?php
use yii\easyii\modules\carousel\api\Carousel;
use yii\easyii\modules\page\api\Page;
use yii\easyii\modules\feedback\api\Feedback;
use yii\easyii\modules\gallery\api\Gallery;

$page = Page::get('page-index');

$asset = \app\assets\AppAsset::register($this);

$this->title = $page->seo('title', $page->model->title);
?>
<nav id="mainNav" class="navbar navbar-default navbar-main navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> Меню <i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand page-scroll logo" href="#page-top"><img src="<?=$asset->baseUrl?>/img/logo.png" alt="uco-logo"></a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-left">
				<li>
					<a class="page-scroll" href="#about">О компании</a>
				</li>
				<li>
					<a class="page-scroll"  href="#partners">Наши партнеры</a>
				</li>
				<li>
					<a class="page-scroll"  href="#clients">Наши клиенты</a>
				</li>
				<li>
					<a class="page-scroll" href="#advant">Наши преимущества</a>
				</li>
				<li>
					<a class="page-scroll" href="#contact">Контакты</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div style="margin-top: 80px">
	<?= Carousel::widget(1920, 1080) ?>
</div>


<section id="about" class="partners_box about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading client-heading"><?=Page::get('page-index')->getTitle()?></h2>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?=Page::get('page-index')->getText()?>
		</div>
	</div>
</section>

<section data-type="background" data-speed="2" class="paralax paralax_white"></section>

<section id="partners" class="partners_box">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-heading client-heading"><?=Page::get('page-partners')->getTitle()?></h2>
			</div>
		</div>
	</div>
	<div class="container">
		<?=Page::get('page-partners')->getText()?>
	</div>
</section>

<section data-type="background" data-speed="10" class="paralax paralax_color"></section>

<section id="clients" class="partners_box clients">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading client-heading">наши клиенты</h2>
			</div>
		</div>
	</div>
	<div class="container">
		<?php foreach(Gallery::last(9) as $photo) : ?>
			<div class="frame">
				<span class="helper"></span><img src="<?= $photo->image ?>" alt="">
			</div>
		<?php endforeach;?>
	</div>
</section>

<section data-type="background" data-speed="2" class="paralax paralax_white"></section>

<section id="advant" class="partners_box">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-heading client-heading"><?=Page::get('page-position')->getTitle()?></h2>
			</div>
		</div>
	</div>
	<div class="container">
		<?=Page::get('page-position')->getText()?>
	</div>
</section>

<div class="city">
	<div class="city-content">
		<div class="city-content-inner">
			<h1 class="city_h1">пишите, звоните, приходите!</h1>
			<div class="inner-btn ">
				<a href="#contact" class="arrow-button-footer page-scroll" ><img class="img-arrow-button-footer" src="<?=$asset->baseUrl?>/img/arrow-button-footer.png"  alt="arrow-button"></a>
				<p class="description d-city">перейти в контакты</p>
			</div>
		</div>
	</div>
</div>

<div id="contact" class="contact_form">
	<div class="container">

		<div class="col-md-12">
			<h2 class="contacts_header uppercase"><?=Page::get('page-contact')->getTitle()?></h2>
			<?=Page::get('page-contact')->getText()?>
		</div>

		<div class="col-md-4">
			<?php if(Yii::$app->request->get(Feedback::SENT_VAR)) : ?>
				<h4 class="text-success"><i class="glyphicon glyphicon-ok"></i> Сообщение отправлено</h4>
			<?php else : ?>
				<?= Feedback::form() ?>
			<?php endif; ?>
		</div>
	</div>
</div>
