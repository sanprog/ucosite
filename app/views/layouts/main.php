<?php

$asset = \app\assets\AppAsset::register($this);

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
<div id="wrapper" class="">

        <?= $content ?>

	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

	<footer class="footer">
		<div class="container">
			<p class="pull-left copyright">Все права защищены. © 2017 · UCO</p>
			<ul class="social-icon animate pull-right">
				<li><a href=""><i class="fa fa-vk" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</footer>

</div>
<?php $this->endContent(); ?>
