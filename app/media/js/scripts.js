(function($) {
	"use strict"; // Start of use strict

	// jQuery for page scrolling feature - requires jQuery Easing plugin
	$(document).on('click', 'a.page-scroll', function(event) {
		var $anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: ($($anchor.attr('href')).offset().top - 50)
		}, 1200);
		event.preventDefault();
	});

	// Highlight the top nav as scrolling occurs
	$('body').scrollspy({
		target: '.navbar-fixed-top',
		offset: 51
	});

	// Closes the Responsive Menu on Menu Item Click
	$('.navbar-collapse ul li a').click(function() {
		$('.navbar-toggle:visible').click();
	});

	// Offset for Main Navigation
	$('#mainNav').affix({
		offset: {
			top: 100
		}
	})


	$(document).ready(function() {
		function heightDetect(){
			$("header").css("height", $(window).height());
		};
//хеадер занимает всю высоту
		heightDetect();
		$(window).resize(function(){
			heightDetect();
		});
	});


	$(document).ready(function(){
		$('section[data-type="background"]').each(function(){
			var $bgobj = $(this); // создаем объект

			$(window).scroll(function() {
				var yPos = -($bgobj.offset().top - $(window).scrollTop() / $bgobj.data('speed')); // вычисляем коэффициент
				// Присваиваем значение background-position
				var coords = 'center '+ yPos + 'px';
				// Создаем эффект Parallax Scrolling
				$bgobj.css({ backgroundPosition: coords });
			});
		});
	});

	$(document).ready(function(){
		var screenHeight = $(window).height();

		$('.carousel-inner').height(screenHeight - 80);
		$('.carousel-inner .item').height(screenHeight - 80);
		$('.carousel img').width('100%');
	});





})(jQuery); // End of use strict
